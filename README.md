# Drupal Scripts

These scripts automate some of our local Drupal development workflow at WebTech.
Namely, building new local Drupal sites from our profile, and syncing existing
production sites from the remote server down to a local development environment.

## Installation

Clone this repository to your vagrant box and run the install script:

```bash
$ ./install.sh
```
