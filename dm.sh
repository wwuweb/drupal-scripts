#!/bin/bash

# ==============================================================================
#
# FILE: dm.sh
#   "drupal make"
#
# USAGE: ./dm.sh <profile_directory> <drupal_root> <database_name>
#
# DESCRIPTION: Make a clean Drupal install with the given profile
#
# ARGUMENTS:
#   profile_directory: The absolute path to the Drupal install profile and
#     makefile to use.
#   drupal_root: The directory location where the site should be installed. This
#     directory must NOT exist prior to installation.
#   database_name: The name of the database to create for the site.
#
# AUTHOR:
#   WebTech 2016
#
# ==============================================================================

set -o nounset

# ------------------------------------------------------------------------------
#
# Prompt the user to continue by entering "y" or "n". Function will display a
# message and terminate the current shell if the user chooses "n". Otherwise it
# will display a different message and continue.
#
# ARGUMENTS:
#   $message The prompt to display to the user.
#   $confirm The message to display when the user confirms.
#   $decline The message to display when the user cancels.
#
# RETURN:
#   None
#
# ------------------------------------------------------------------------------
prompt()
{
  local message="$1"
  local confirm="${2:-}"
  local decline="${3:-}"

  trap "printf \"${decline}\"" EXIT

  while true; do
    read -p "${message} (y/n) " choice

    case "$choice" in
      y|Y )
        break
        ;;
      n|N )
        exit
        ;;
    esac
  done

  printf "$confirm"

  trap - EXIT
}

__main()
{
  if [ $# -ne 3 ]; then
    echo "Wrong number of arguments"
    echo "Usage: <profile_directory> <drupal_root> <database_name>"
    exit 1
  fi

  # ==============================================================================
  #
  # VARIABLES
  #
  # ==============================================================================

  # INSTALL LOCATION
  readonly drupal_root="${2%/}"

  # INSTALL PROFILE
  readonly profile_directory="${1%/}"
  readonly profile=$(basename "$profile_directory")

  # DATABASE
  readonly db_name="${3}"
  readonly db_user="root"
  readonly db_pass="root"
  readonly db_host="localhost"
  readonly db_url="mysql://${db_user}:${db_pass}@${db_host}/${db_name}"

  # SITE
  readonly site_name="$profile"
  readonly site_mail="drupalbot@wwu.edu"

  # ACCOUNT
  readonly account_name="admin"
  readonly account_pass="admin"
  readonly account_mail="drupalbot@wwu.edu"

  # ==============================================================================
  #
  # VALIDATION
  #
  # ==============================================================================

  if [ -d "$drupal_root" ]; then
    echo "Drupal root directory already exists."

    prompt "Delete existing directory and continue?" \
      "Deleting root directory ...\n" \
      "Exiting without building makefile.\n"

    (
    chmod 0777 $drupal_root \
      --recursive

    rm "$drupal_root" \
      --recursive \
      --force

    if [ $? -eq 0 ]; then
      echo "Root directory deleted successfully."
    else
      echo "Failed to delete root directory."
    fi
    ) &
  fi

  # ==============================================================================
  #
  # DATABASE
  #
  # ==============================================================================

  printf "Creating database \"%s\" ...\n" "$db_name"

  mysql -e "CREATE DATABASE ${db_name}" 2>/dev/null

  if [ $? -ne 0 ]; then
    echo "Database already exists."

    prompt "Drop existing database and continue?"

    mysql -e "DROP DATABASE IF EXISTS ${db_name}; CREATE DATABASE ${db_name}"
  fi

  if [ $? -eq 0 ]; then
    echo "Database created successfully."
  else
    echo "Failed to create database."
    exit 1
  fi

  # ==============================================================================
  #
  # MAKE
  #
  # ==============================================================================

  wait

  drush make "${profile_directory}/${profile}.make" "$drupal_root"

  if [ $? -eq 0 ]; then
    echo "Makefile built successfully."
  else
    echo "Makefile build failed."
    exit 1
  fi

  echo "Copying profile into Drupal installation ..."

  cp -R "$profile_directory" "${drupal_root}/profiles"

  if [ $? -eq 0 ]; then
    echo "Profile copied successfully."
  else
    echo "Profile failed to copy."
    exit 1
  fi

  # ==============================================================================
  #
  # SITE INSTALL
  #
  # ==============================================================================

  prompt "Install Drupal with built profile?" \
    "Installing drupal ...\n" \
    "Skipping installation.\n"

  cd "$drupal_root"

  export PHP_OPTIONS="-d sendmail_path=$(which true)"

  drush --yes site-install "$profile" \
    --db-url="$db_url" \
    --site-name="$site_name" \
    --site-mail="$site_mail" \
    --account-mail="$account_mail" \
    --account-name="$account_name" \
    --account-pass="$account_pass"

  if [ $? -eq 0 ]; then
    echo "Installation complete."
  else
    echo "Installation failed."
    exit 1
  fi
}

__main $@
