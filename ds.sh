#!/bin/bash

# ==============================================================================
#
# FILE: ds.sh
#   "drupal sync"
#
# USAGE: ./ds.sh <subdomain>
#
# DESCRIPTION: Sync a Drupal site from remote production to local development
#   using existing, locally-defined drush aliases.
#
#   This script can sync the site down from scratch, or it can be used to
#   freshen an existing local copy. The database, configuration, and file
#   structure will be recreated as needed. Symlinked themes will NOT be synced,
#   so make sure that these are up to date and compiled.
#
# ARGUMENTS:
#   subdomain: The subdomain of the site to sync. For a site "fqdn.wwu.edu", the
#     subdomain would be "fqdn".
#
# AUTHOR:
#   WebTech 2016
#
# ==============================================================================

set -o nounset

# ------------------------------------------------------------------------------
#
# Check if a given Drush alias is defined.
#
# ARGUMENTS:
#   $alias The Drush alias to check for exitance.
#
# RETURN:
#   1 if the alias is not found, 0 otherwise.
#
# ------------------------------------------------------------------------------
alias_exists()
{
  local alias="${1#@}"
  local count=$(drush site-alias | grep -xc $alias)

  if [ $count -eq 0 ]; then
    return 1;
  else
    return 0;
  fi
}

# ------------------------------------------------------------------------------
#
# Print a warning to the user about the actions taken in this script.
#
# ARGUMENTS:
#   None
#
# RETURN:
#   None
#
# ------------------------------------------------------------------------------
print_warning()
{
cat << EOF

********************************************************************************

WARNING: This script will completely overwrite the database of your local Drupal
installation, as well as modules and uploaded site files. If you have
in-progress work that you would like to save, use the Backup Migrate module to
create a complete site backup to another location on your system.

Symlinks to your local filesystem will NOT be overwritten. Thus, if you have
theme or module repositories symlinked into the site install, these will be be
preserved along with their contents.

********************************************************************************

EOF
}

# ------------------------------------------------------------------------------
#
# Prompt the user to choose from a list of options.
#
# ARGUMENTS:
#   $message The prompt to display to the user. This should contain the list of
#     choices for the user to choose from.
#   $choices The list of options available to the user.
#
# RETURN:
#   The chosen option.
#
# ------------------------------------------------------------------------------
choose()
{
  local message="$1"
  local choices="^(${2// /|})$"
  local valid=false

  while ! $valid; do
    read -p "$message " choice

    if [[ "$choice" =~ $choices ]]; then
      valid=true
    fi
  done

  echo $choice
}

# ------------------------------------------------------------------------------
#
# Prompt the user to continue by entering "y" or "n". Function will display a
# message and terminate the current shell if the user chooses "n". Otherwise it
# will display a different message and continue.
#
# ARGUMENTS:
#   $message The prompt to display to the user.
#   $confirm The message to display when the user confirms.
#   $decline The message to display when the user cancels.
#
# RETURN:
#   None
#
# ------------------------------------------------------------------------------
prompt()
{
  local message="$1"
  local confirm="${2-''}"
  local decline="${3-''}"

  trap "echo '${decline}'" EXIT

  while true; do
    read -p "${message} (y/n): " choice

    case $choice in
      y|Y )
        break
        ;;
      n|N )
        exit
        ;;
    esac
  done

  echo $confirm

  trap - EXIT
}

__main()
{
  if [ $# -ne 1 ]; then
    echo "Wrong number of arguments"
    echo "Usage: <subdomain>"
    exit 1
  fi

  # ==============================================================================
  #
  # VARIABLES
  #
  # ==============================================================================

  # SITE SUBDOMAIN
  readonly subdomain="$1"

  # ENVIRONMENT
  readonly environment=$(choose "Choose the environment from which to sync (dev/test/prod):" "dev test prod")

  # INSTALL LOCATION
  readonly drupal_root="/vagrant/sites/${subdomain}"

  # DRUSH ALIASES
  readonly remote_alias="@${subdomain}.${environment}"
  readonly local_alias="@${subdomain}.local"

  # POLICY FILE
  readonly policy_file=~/.drush/policy.drush.inc

  # DATABASE
  readonly db_name=$(echo $subdomain | sed 's/[^a-zA-Z0-9]//g')
  readonly db_user="drupal"
  readonly db_pass="drupal"
  readonly db_host="localhost"
  readonly db_url="mysql://${db_user}:${db_pass}@${db_host}/${db_name}"

  # SETTINGS
  readonly default_settings="${drupal_root}/sites/default/default.settings.php"
  readonly settings="${drupal_root}/sites/default/settings.php"

  # SITE
  readonly site_name="$subdomain"
  readonly site_mail="drupalbot@wwu.edu"

  # ACCOUNT
  readonly account_name="admin"
  readonly account_pass="admin"
  readonly account_mail="drupalbot@wwu.edu"

  # FORCE DATABASE SYNC
  local force_database_sync=false

  # ==============================================================================
  #
  # VALIDATE
  #
  # ==============================================================================

  if [ ! -e "$policy_file" ] || [ ! -L "$policy_file" ]; then
    echo "Drush policy file does not exist. Run install.sh."
    exit 1
  fi

  if ! alias_exists "$remote_alias"; then
    echo "Alias ${remote_alias} does not exist."
    exit 1
  fi

  if ! alias_exists "$local_alias"; then
    echo "Alias ${local_alias} does not exist."
    exit 1;
  fi

  print_warning

  if [ "$environment" = "dev" ] || [ "$environment" = "test" ]; then
    local url="${subdomain}${environment}.wwu.edu"
  else
    local url="${subdomain}.wwu.edu"
  fi

  # ==============================================================================
  #
  # CORE
  #
  # ==============================================================================

  prompt "Do you want to continue syncing ${url}?" \
    "Syncing core..." \
    "Exiting without syncing."

  drush --yes --root=${drupal_root} core-status --projects=sites,files > /dev/null 2>&1

  if [ $? -ne 0 ]; then
    rm -rf ${drupal_root}
  fi

  if [ -d "$drupal_root" ]; then
    find "$drupal_root" -type d -exec chmod 0755 {} \;
    find "$drupal_root" -type f -exec chmod 0644 {} \;
  fi

  drush --yes core-rsync \
    "$remote_alias" \
    "$local_alias" \
    --exclude-sites \
    --exclude-files \
    --prune-empty-dirs \
    --keep-dirlinks

  if [ $? -eq 0 ]; then
    echo "Drupal core synced successfully."
  else
    echo "Drupal core failed to sync."
    exit 1
  fi

  # ==============================================================================
  #
  # MODULES
  #
  # ==============================================================================

  echo "Syncing site modules ..."

  if [ ! -d "${drupal_root}/sites/default/modules" ]; then
    mkdir -p "${drupal_root}/sites/default/modules"
  fi

  if [ $? -eq 0 ]; then
    drush --yes core-rsync \
      "${remote_alias}:%modules/" \
      "${local_alias}:sites/default/modules" \
      --prune-empty-dirs \
      --keep-dirlinks
  fi

  if [ $? -eq 0 ]; then
    echo "Site modules synced successfully."
  else
    echo "Site modules failed to sync."
  fi

  # ==============================================================================
  #
  # SITE INSTALL
  #
  # ==============================================================================

  if [ ! -f "$default_settings" ]; then
    install /dev/null "$default_settings" \
      -D \
      --verbose

    if [ $? -ne 0 ]; then
      echo "Failed to create default.settings.php, which is needed to install drupal."
      exit 1
    fi
  fi

  if [ ! -f "$settings" ]; then
    install /dev/null "$settings" \
      -D \
      --verbose

    if [ $? -ne 0 ]; then
      echo "Failed to create settings.php, which is needed to install drupal."
      exit 1
    fi

    echo "Installing drupal ..."

    cd "$drupal_root"

    export PHP_OPTIONS="-d sendmail_path=$(which true)"

    drush --yes site-install "minimal" \
      --db-url="$db_url" \
      --site-name="$site_name" \
      --site-mail="$site_mail" \
      --account-mail="$account_mail" \
      --account-name="$account_name" \
      --account-pass="$account_pass"

    if [ $? -eq 0 ]; then
      echo "Site installed successfully."
    else
      echo "Site failed to install."
      exit 1
    fi

    force_database_sync=true
  fi

  # ==============================================================================
  #
  # DATABASE
  #
  # ==============================================================================

  (

  if [ $force_database_sync = false ]; then
    prompt "Do you want to sync the database?" \
      "Syncing database..." \
      "Skipping database sync."
  fi

  drush --yes sql-sync "$remote_alias" "$local_alias" \
    --temp \
    --create-db \
    --structure-tables-list=cache,cache_*,history,search_*,sessions,watchdog

  if [ $? -eq 0 ]; then
    echo "Database synced successfully."
  else
    echo "Database failed to sync."
    exit 1
  fi

  )

  # ==============================================================================
  #
  # CLEAR CACHES
  #
  # ==============================================================================

  echo "Rebuilding registry and clearing cache ..."

  drush "$local_alias" --yes registry-rebuild \
    --fire-bazooka

  # ==============================================================================
  #
  # SETTINGS
  #
  # ==============================================================================

  echo "Setting site configuration ..."

  drush "$local_alias" --yes vset --exact cache                  "0" &
  drush "$local_alias" --yes vset --exact block_cache            "0" &
  drush "$local_alias" --yes vset --exact cache_lifetime         "0" &
  drush "$local_alias" --yes vset --exact page_cache_maximum_age "0" &
  drush "$local_alias" --yes vset --exact preprocess_css         "0" &
  drush "$local_alias" --yes vset --exact preprocess_js          "0" &
  drush "$local_alias" --yes vset --exact page_compression       "0" &
  drush "$local_alias" --yes vset --exact file_public_path       "sites/default/files" &
  drush "$local_alias" --yes vset --exact file_temporary_path    "/tmp" &

  wait

  # ==============================================================================
  #
  # FILES
  #
  # ==============================================================================

  drush "$local_alias" --yes dis wwu_clean_file_urls &
  drush "$local_alias" --yes en stage_file_proxy
  drush "$local_alias" --yes vset --exact stage_file_proxy_origin_dir "files"
  drush "$local_alias" --yes vset --exact stage_file_proxy_origin     "https://${url}"

  echo "Sync complete."
}

__main $@
