#!/bin/bash

# ==============================================================================
#
# FILE: install.sh
#
# USAGE: ./install.sh
#
# DESCRIPTION: Perform installation tasks. This includes installing the drush
#   policy file, which prevents syncing from local to production accidentally.
#
# AUTHOR:
#   WebTech 2017
#
# ==============================================================================

set -o nounset

# ==============================================================================
#
# VARIABLES
#
# ==============================================================================

# SCRIPT DIRECTORY
readonly __DIR__=$(cd $(dirname ${BASH_SOURCE[0]}) && pwd)

# BIN DIRECTORY
readonly bin_path=/usr/local/bin

# INCLUDE PATH
readonly include_path=~/.drush

# ==============================================================================
#
# SYMLINK EXECUTABLES
#
# ==============================================================================

sudo ln \
  --symbolic \
  --force \
  "${__DIR__}/ds.sh" "${bin_path}/ds"

if [ $? -eq 0 ]; then
  echo "Succesfully installed ds script."
else
  echo "Failed to install ds script located at ${__DIR__}/ds.sh in ${bin_path}"
  exit 1
fi

sudo ln \
  --symbolic \
  --force \
  "${__DIR__}/dm.sh" "${bin_path}/dm"

if [ $? -eq 0 ]; then
  echo "Succesfully installed dm script."
else
  echo "Failed to install ds script located at ${__DIR__}/dm.sh in ${bin_path}"
  exit 1
fi

# ==============================================================================
#
# CREATE DRUSH DIRECTORY
#
# ==============================================================================

if [ -f "$include_path" ]; then
  rm -f $include_path

  if [ $? -ne 0 ]; then
    echo "Failed to delete ${include_path} while creating the drush directory."
    exit 1
  fi
fi

if [ ! -d "$include_path" ]; then
  echo "Creating drush directory ..."

  mkdir $include_path

  if [ $? -eq 0 ]; then
    echo "Successfully created drush directory."
  else
    echo "Failed to create drush directory at ${include_path}."
    exit 1
  fi
fi

# ==============================================================================
#
# CREATE POLICY FILE
#
# ==============================================================================

echo "Creating drush policy file ..."

ln \
  --symbolic \
  --force \
  "${__DIR__}/policy.drush.inc" "$include_path/policy.drush.inc" \

if [ $? -eq 0 ]; then
  echo "Drush policy file created."
else
  echo "Could not create policy file at ${include_path}/policy.drush.inc."
  exit 1
fi

# ==============================================================================
#
# INSTALL REGISTRY REBUILD
#
# ==============================================================================

if [ ! -f "${include_path}/registry_rebuild/registry_rebuild.php" ]; then
  drush --yes pm-download registry_rebuild

  if [ $? -eq 0 ]; then
    echo "Drush registry-rebuild installed."
  else
    echo "Could not install drush registry-rebuild."
    exit 1
  fi
fi

drush --yes --quiet cc drush

echo "Installation successful."
