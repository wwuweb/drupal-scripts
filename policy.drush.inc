<?php
/**
 * @file
 * Drush policy file
 *
 * Provides rules to validate drush commands.
 */

/**
 * Implements drush_hook_COMMAND_validate().
 */
function drush_policy_sql_sync_validate($source = NULL, $destination = NULL) {
  if (preg_match("/^@prod/", $destination)) {
    return drush_set_error('POLICY_DENY', dt('Attempted to overwrite the production database. Aborting.'));
  }
}

/**
 * Implements drush_hook_COMMAND_validate().
 */
function drush_policy_core_rsync_validate($source = NULL, $destination = NULL) {
  if (preg_match("/^@prod/", $destination)) {
    return drush_set_error('POLICY_DENY', dt('Attempted to overwrite the production file structure. Aborting.'));
  }
}
